package com.liodevel.liacembdr;

import android.provider.CalendarContract;

import com.google.api.services.calendar.CalendarScopes;

/**
 * Created by emilio on 12/04/16.
 */
public class Statics {

    public static final int TYPE_DAY_VIEW = 1;
    public static final int TYPE_THREE_DAY_VIEW = 2;
    public static final int TYPE_WEEK_VIEW = 3;

    public static final int REQUEST_ACCOUNT_PICKER = 1000;
    public static final int REQUEST_AUTHORIZATION = 1001;
    public static final int REQUEST_GOOGLE_PLAY_SERVICES = 1002;
    public static final int REQUEST_NEW_EVENT = 1003;
    public static final String PREF_ACCOUNT_NAME = "accountName";

    public static final String[] SCOPES = {CalendarScopes.CALENDAR_READONLY};

    public static final String DEBUG_TAG = "LIA-";
    public static final String[] INSTANCE_PROJECTION = new String[]{
            CalendarContract.Instances.EVENT_ID,        // 0
            CalendarContract.Instances.BEGIN,           // 1
            CalendarContract.Instances.END,             // 2
            CalendarContract.Instances.EVENT_COLOR,     // 3
            CalendarContract.Instances.TITLE,           // 4
            CalendarContract.Instances.EVENT_LOCATION   // 5
    };

    // Projections de encima
    public static final int PROJECTION_ID_INDEX = 0;
    public static final int PROJECTION_BEGIN_INDEX = 1;
    public static final int PROJECTION_END_INDEX = 2;
    public static final int PROJECTION_EVENT_COLOR = 3;
    public static final int PROJECTION_TITLE_INDEX = 4;
    public static final int PROJECTION_LOCATION = 5;

}
