package com.liodevel.liacembdr;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GoogleAccountCredential;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.googleapis.util.*;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.ExponentialBackOff;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.*;
import android.accounts.AccountManager;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.alamkanak.weekview.DateTimeInterpreter;
import com.alamkanak.weekview.MonthLoader;
import com.alamkanak.weekview.WeekView;
import com.alamkanak.weekview.WeekViewEvent;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import static com.liodevel.liacembdr.Server.getEvents;

public class MainActivity
        extends AppCompatActivity
        implements WeekView.EventClickListener,MonthLoader.MonthChangeListener,WeekView.EventLongPressListener,WeekView.EmptyViewLongPressListener,WeekView.EmptyViewClickListener {

    Context context;
    private Boolean exit = false;

    // WEEKVIEW
    WeekView mWeekView;

    private int mWeekViewType = Statics.TYPE_THREE_DAY_VIEW;

    // GOOGLE CALENDAR
    GoogleAccountCredential mCredential;
    ContentResolver cr;

    List<WeekViewEvent> events;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        events = new ArrayList<>();
        context = this;
        cr = getContentResolver();

        mWeekView = (WeekView) findViewById(R.id.weekView);

        mWeekView.setOnEventClickListener(this);
        mWeekView.setEventLongPressListener(this);
        mWeekView.setEmptyViewClickListener(this);
        mWeekView.setEmptyViewLongPressListener(this);
        setupDateTimeInterpreter();
        mWeekView.setMonthChangeListener(new MonthLoader.MonthChangeListener() {
            @Override
            public List<WeekViewEvent> onMonthChange(int newYear, int newMonth) {

                List<WeekViewEvent> ret = new ArrayList<>();

                Log.i("LIA", "OnMonthChange");
                Log.i("LIA", "Mes:" + newMonth);
                Log.i("LIA", "Año:" + newYear);
                if (mCredential.getSelectedAccountName() != null) {
                    Log.i("LIA", mCredential.getSelectedAccountName());
                    ret = getEvents(newMonth, newYear, cr, mCredential, context);
                }

                return ret;
            }
        });


        // Inicializar credenciales
        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
        mCredential = GoogleAccountCredential.usingOAuth2(
                getApplicationContext(), Arrays.asList(Statics.SCOPES))
                .setBackOff(new ExponentialBackOff())
                .setSelectedAccountName(settings.getString(Statics.PREF_ACCOUNT_NAME, null));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //setupDateTimeInterpreter(id == R.id.action_week_view);
        switch (id) {
            case R.id.action_today:
                mWeekView.goToToday();
                return true;
            case R.id.action_day_view:
                if (mWeekViewType != Statics.TYPE_DAY_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = Statics.TYPE_DAY_VIEW;
                    mWeekView.setNumberOfVisibleDays(1);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 12, getResources().getDisplayMetrics()));
                }
                return true;
            case R.id.action_week_view:
                if (mWeekViewType != Statics.TYPE_WEEK_VIEW) {
                    item.setChecked(!item.isChecked());
                    mWeekViewType = Statics.TYPE_WEEK_VIEW;
                    mWeekView.setNumberOfVisibleDays(7);

                    // Lets change some dimensions to best fit the view.
                    mWeekView.setColumnGap((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources().getDisplayMetrics()));
                    mWeekView.setTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                    mWeekView.setEventTextSize((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 10, getResources().getDisplayMetrics()));
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onEmptyViewClicked(final Calendar time) {
        // Toast.makeText(this, "Clicked " + time.toString(), Toast.LENGTH_SHORT).show();

        // Abrir EventActivity
        Intent eventIntent = new Intent(this, EventActivity.class);

        eventIntent.putExtra("edicion", false);
        eventIntent.putExtra("year", time.get(Calendar.YEAR));
        eventIntent.putExtra("month", time.get(Calendar.MONTH));
        eventIntent.putExtra("day", time.get(Calendar.DAY_OF_MONTH));
        eventIntent.putExtra("selected_account", mCredential.getSelectedAccountName());

        startActivityForResult(eventIntent, Statics.REQUEST_NEW_EVENT);

        //startActivity(eventIntent);

    }

    @Override
    public void onEmptyViewLongPress(Calendar time) {
        //Toast.makeText(this, "LongClicked " + time.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onEventClick(WeekViewEvent event, RectF eventRect) {
        //ABRIR INFO o EDITAR
        //Toast.makeText(this, "Clicked " + event.getId() + "-"  + event.getName(), Toast.LENGTH_SHORT).show();
        // Abrir EventActivity
        Intent eventIntent = new Intent(this, EventActivity.class);

        boolean horaExtra = false;
        if (event.getLocation().contains("X")){
            horaExtra = true;
        }

        eventIntent.putExtra("edicion", true);
        eventIntent.putExtra("id", event.getId());
        try {
            eventIntent.putExtra("titulo", event.getName().split("\\(")[1].split("\\)")[0]);
        } catch (Exception e) {
            eventIntent.putExtra("titulo", "");
        }
        eventIntent.putExtra("tipo", event.getName().split("\\(")[0]);
        eventIntent.putExtra("fecha_inicio", event.getStartTime().getTimeInMillis());
        eventIntent.putExtra("fecha_fin", event.getEndTime().getTimeInMillis());
        eventIntent.putExtra("hora_extra", horaExtra);

        eventIntent.putExtra("selected_account", mCredential.getSelectedAccountName());


        startActivityForResult(eventIntent, Statics.REQUEST_NEW_EVENT);

        //startActivity(eventIntent);
    }

    @Override
    public void onEventLongPress(final WeekViewEvent event, RectF eventRect) {
        // BORRAR
        //Toast.makeText(this, "LongClicked: " + event.getId() + "-" + event.getName(), Toast.LENGTH_SHORT).show();
        final long id = event.getId();

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(getResources().getString(R.string.borrar_evento) + " " + event.getName())
                .setPositiveButton(getResources().getString(R.string.si), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (Server.borrarEvento(event.getId(), getContentResolver(), context) == true){
                            Toast.makeText(context, event.getName() + " borrado ", Toast.LENGTH_SHORT).show();
                            // Refrescar calendario

                            events.remove(Utils.getIndexById(event.getId(), events));
                            mWeekView.notifyDatasetChanged();
                        }

                        dialog.cancel();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }


    @Override
    public List<? extends WeekViewEvent> onMonthChange(int newYear, int newMonth) {
        Toast.makeText(this, "MONTH MonthChangeListener: " + newYear + ", " + newMonth, Toast.LENGTH_SHORT).show();
        return null;
    }


    /**
     * Prepara el intérprete de fechas
     */
    private void setupDateTimeInterpreter() {
        mWeekView.setDateTimeInterpreter(new DateTimeInterpreter() {
            @Override
            public String interpretDate(Calendar date) {
                SimpleDateFormat weekdayNameFormat = new SimpleDateFormat("EEE", Locale.getDefault());
                String weekday = weekdayNameFormat.format(date.getTime());
                SimpleDateFormat format = new SimpleDateFormat(" d/M", Locale.getDefault());

                // All android api level do not have a standard way of getting the first letter of
                // the week day name. Hence we get the first char programmatically.
                // Details: http://stackoverflow.com/questions/16959502/get-one-letter-abbreviation-of-week-day-of-a-date-in-java#answer-16959657
                //if (shortDate)
                //    weekday = String.valueOf(weekday.charAt(0));
                return weekday.toUpperCase() + format.format(date.getTime());
            }

            @Override
            public String interpretTime(int hour) {
                return String.valueOf(hour);
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (isGooglePlayServicesAvailable()) {
            refreshResults();

        } else {
            //mOutputText.setText("Google Play Services required: " +
            //        "after installing, close and relaunch this app.");
        }
    }

    /**
     * Called when an activity launched here (specifically, AccountPicker
     * and authorization) exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     * @param requestCode code indicating which activity result is incoming.
     * @param resultCode code indicating the result of the incoming
     *     activity result.
     * @param data Intent (containing result data) returned by incoming
     *     activity result.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Statics.REQUEST_GOOGLE_PLAY_SERVICES:
                if (resultCode != RESULT_OK) {
                    isGooglePlayServicesAvailable();
                }
                break;
            case Statics.REQUEST_ACCOUNT_PICKER:
                if (resultCode == RESULT_OK && data != null &&
                        data.getExtras() != null) {
                    String accountName =
                            data.getStringExtra(AccountManager.KEY_ACCOUNT_NAME);
                    if (accountName != null) {
                        mCredential.setSelectedAccountName(accountName);
                        SharedPreferences settings = getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = settings.edit();
                        editor.putString(Statics.PREF_ACCOUNT_NAME, accountName);
                        Log.i("LIA", "PREFS_ACCOUNT_NAME: " + accountName);
                        editor.apply();
                    }
                } else if (resultCode == RESULT_CANCELED) {
                    //mOutputText.setText("Account unspecified.");
                }
                break;
            case Statics.REQUEST_AUTHORIZATION:
                if (resultCode != RESULT_OK) {
                    chooseAccount();
                }
                break;

            case Statics.REQUEST_NEW_EVENT:
                //if (resultCode != RESULT_OK) {
                    // GetExtras
                    try {
                        final boolean saved = data.getExtras().getBoolean("saved");

                        if (saved) {
                            Log.i("LIA", "--GUARDADO--");
                            String tipo = "";
                            String titulo = "";
                            long longStart = 0;
                            long longEnd = 0;
                            long id = 0;

                            try {
                                tipo = data.getExtras().getString("tipo");
                                Log.i(Statics.DEBUG_TAG, "tipo: " + tipo);
                                titulo = data.getExtras().getString("titulo");
                                Log.i(Statics.DEBUG_TAG, "titulo: " + titulo);
                                longStart = data.getExtras().getLong("inicio");
                                Log.i(Statics.DEBUG_TAG, "inicio: " + longStart);
                                longEnd = data.getExtras().getLong("fin");
                                Log.i(Statics.DEBUG_TAG, "fin: " + longEnd);
                                id = data.getExtras().getLong("id");
                                Log.i(Statics.DEBUG_TAG, "id: " + id);


                            } catch (Exception e){
                                Log.e(Statics.DEBUG_TAG, "Error getExtras: " + e.toString());
                            }

                            WeekViewEvent newEvent = new WeekViewEvent();
                            newEvent.setName(tipo + "(" + titulo + ")");
                            Calendar start = Calendar.getInstance();
                            Calendar end = Calendar.getInstance();
                            start.setTimeInMillis(longStart);
                            end.setTimeInMillis(longEnd);
                            newEvent.setStartTime(start);
                            newEvent.setId(id);

                            events.add(newEvent);

                            mWeekView.notifyDatasetChanged();
                        }
                    } catch (Exception e){
                        Log.e(Statics.DEBUG_TAG, "Error getSaved: " + e.toString());

                    }

                //}
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Attempt to get a set of data from the Google Calendar API to display. If the
     * email address isn't known yet, then call chooseAccount() method so the
     * user can pick an account.
     */
    private void refreshResults() {
        if (mCredential.getSelectedAccountName() == null) {
            chooseAccount();
        } else {
            if (isDeviceOnline()) {
                new MakeRequestTask(mCredential).execute();

            } else {
                Toast.makeText(context, "Sin conexión", Toast.LENGTH_SHORT).show();

            }
        }
    }

    /**
     * Escoger cuenta de google calendar
     */
    private void chooseAccount() {
        //Log.i("LIA", mCredential.getSelectedAccount().toString());

        startActivityForResult(
                mCredential.newChooseAccountIntent(), Statics.REQUEST_ACCOUNT_PICKER);
    }

    /**
     * Comprueba si hay conexión
     * @return true si hay conectividad, false en caso contrario.
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Comprueba si Google Play Services está disponible
     * @return true si está disponible; false en caso contrario.
     */
    private boolean isGooglePlayServicesAvailable() {
        final int connectionStatusCode =
                GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);
        if (GooglePlayServicesUtil.isUserRecoverableError(connectionStatusCode)) {
            showGooglePlayServicesAvailabilityErrorDialog(connectionStatusCode);
            return false;
        } else if (connectionStatusCode != ConnectionResult.SUCCESS) {
            return false;
        }
        return true;
    }

    /**
     * Diálogo de error por Google Play Services
     * @param connectionStatusCode code describing the presence (or lack of)
     *     Google Play Services on this device.
     */
    void showGooglePlayServicesAvailabilityErrorDialog(
            final int connectionStatusCode) {
        Dialog dialog = GooglePlayServicesUtil.getErrorDialog(
                connectionStatusCode,
                MainActivity.this,
                Statics.REQUEST_GOOGLE_PLAY_SERVICES);
        dialog.show();
    }


    /**
     * Gestiona las llamadas a la API de Google
     */
    private class MakeRequestTask extends AsyncTask<Void, Void, List<String>> {
        private com.google.api.services.calendar.Calendar mService = null;
        private Exception mLastError = null;

        public MakeRequestTask(GoogleAccountCredential credential) {
            HttpTransport transport = AndroidHttp.newCompatibleTransport();
            JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
            mService = new com.google.api.services.calendar.Calendar.Builder(
                    transport, jsonFactory, credential)
                    .setApplicationName("Google Calendar API Android Quickstart")
                    .build();
        }

        /**
         * Background task to call Google Calendar API.
         * @param params no parameters needed for this task.
         */
        @Override
        protected List<String> doInBackground(Void... params) {
            try {
                return getDataFromApi();
            } catch (Exception e) {
                mLastError = e;
                cancel(true);
                return null;
            }
        }

        /**
         * Fetch a list of the next 10 events from the primary calendar.
         * @return List of Strings describing returned events.
         * @throws IOException
         */
        private List<String> getDataFromApi() throws IOException {
            // List the next 10 events from the primary calendar.
            DateTime now = new DateTime(System.currentTimeMillis());
            List<String> eventStrings = new ArrayList<String>();
            Events events = mService.events().list("primary")
                    .setMaxResults(10)
                    .setTimeMin(now)
                    .setOrderBy("startTime")
                    .setSingleEvents(true)
                    .execute();
            List<Event> items = events.getItems();

            for (Event event : items) {
                DateTime start = event.getStart().getDateTime();
                if (start == null) {
                    // All-day events don't have start times, so just use
                    // the start date.
                    start = event.getStart().getDate();
                }
                eventStrings.add(
                        String.format("%s (%s)", event.getSummary(), start));
            }
            return eventStrings;
        }


        @Override
        protected void onPreExecute() {
            //mOutputText.setText("");
            //mProgress.show();
        }

        @Override
        protected void onPostExecute(List<String> output) {
            //mProgress.hide();
            if (output == null || output.size() == 0) {
                //mOutputText.setText("No results returned.");
            } else {
                output.add(0, "Data retrieved using the Google Calendar API:");
                //mOutputText.setText(TextUtils.join("\n", output));
            }
        }

        @Override
        protected void onCancelled() {
            //mProgress.hide();
            if (mLastError != null) {
                if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                    showGooglePlayServicesAvailabilityErrorDialog(
                            ((GooglePlayServicesAvailabilityIOException) mLastError)
                                    .getConnectionStatusCode());
                } else if (mLastError instanceof UserRecoverableAuthIOException) {
                    startActivityForResult(
                            ((UserRecoverableAuthIOException) mLastError).getIntent(),
                            Statics.REQUEST_AUTHORIZATION);
                } else {
                    //mOutputText.setText("The following error occurred:\n"
                    //+ mLastError.getMessage());
                }
            } else {
                //mOutputText.setText("Request cancelled.");
            }
        }
    }


    /**
     * Exit
     */
    @Override
    public void onBackPressed() {

        if (exit) {
            finish(); // finish activity
        } else {
            Toast.makeText(this, "Pulsa otra vez para salir", Toast.LENGTH_LONG).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }




}
